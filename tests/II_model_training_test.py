import datasets
import pandas as pd
from datasets import Dataset, DatasetDict

import sys 
import os 
sys.path.append(os.path.abspath('.')) 
sys.path.append(os.path.abspath('..')) 

from II_model_training import wg_trainer
from I_datapreprocessing_test import TestData
class TestModel:
    def setup_method(self):
        dataloader = TestData()
        self.test_ds = dataloader.get_test_dataset()
        args = {'data_seed':888, 
                'batch_size': 1, 
                'eval_steps': 2, 
                'model_name': "w11wo/javanese-roberta-small-imdb-classifier", 
                'patience': 3, 
                'output_dir': "/mlflow/runs/IMDB",
                'test_mode': True,
                'dataset_name': 'imdb',
                'exp_name': 'IMDB',
                }
        self.wg_trainer = wg_trainer(**args)
    def test_train(self):
        self.wg_trainer.train(self.test_ds)
    def test_mlflow_register(self):
        self.wg_trainer.mlflow_register()
    def test_registered_model_test_available(self):
        print(self.wg_trainer.registered_model_test_available())
        assert self.wg_trainer.registered_model_test_available() is not None
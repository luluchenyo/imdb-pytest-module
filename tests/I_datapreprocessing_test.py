import datasets
import pandas as pd
from datasets import Dataset, DatasetDict

import sys 
import os 
sys.path.append(os.path.abspath('.')) 
from I_datapreprocessing import load_train_test_dataset, tokenizer
class TestData:
    def setup_method(self):
        self.test_ds = self.get_test_data()
    def get_test_data(self):
        dft = pd.DataFrame({'text': ['爛得要死', '太讚了吧', 'so crazy', 'pokemon go', 'wowowowowowowowo'], 'label': [0, 1, 0, 1, 1]})
        dst = Dataset.from_pandas(dft)
        dfv = pd.DataFrame({'text': ['爛得可以', '不能再更棒了', 'so amazing', 'freak', 'hahahahahaha'], 'label': [0, 1, 1, 0, 1]})
        dsv = Dataset.from_pandas(dfv)
        ds = DatasetDict()
        ds['train'] = dst
        ds['test'] = dsv
        return ds
    def test_load_train_test_dataset(self):
        out = load_train_test_dataset()
        assert isinstance(out, type(self.test_ds))
    def test_tokenizer(self):
        self.tokenizered_ds = tokenizer(self.test_ds)
        assert isinstance(self.tokenizered_ds, type(self.test_ds))
        assert 'input_ids' in self.tokenizered_ds['train'].features
    def get_test_dataset(self):
        self.setup_method()
        return tokenizer(self.test_ds)
        
        

    
from datasets import load_dataset, DatasetDict
from transformers import AutoTokenizer
import datasets

def load_train_test_dataset(dataset_name="imdb", split_portion={'train': 0.01, 'test': 0.01}):
    split = [f'{data}[:{int(portion*100)}%]' for data, portion in split_portion.items()]
    ds = load_dataset(dataset_name, split=split)
    ds = DatasetDict({"train":ds[0], "test":ds[1]})
    return ds

def tokenizer(dataset, model_name="w11wo/javanese-roberta-small-imdb-classifier"):
    def tokenize(examples):
        tokenizer = AutoTokenizer.from_pretrained(model_name)
        outputs = tokenizer(examples['text'], truncation=True)
        return outputs
    return dataset.map(tokenize, batched=True)

if __name__ == "__main__":
    out = load_train_test_dataset()
    print(tokenizer(out)[''])